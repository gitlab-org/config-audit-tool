# frozen_string_literal: true

# Configure the Geo secondary role and the PostgreSQL role
roles %w[geo_secondary_role postgres_role]

# Pgbouncer configuration
pgbouncer['enable'] = true
pgbouncer['auth_type'] = 'trust'
pgbouncer['admin_users'] = %w[pgbouncer gitlab-consul]

pgbouncer['users'] = {
  'gitlab-consul': {
    password: __CONSUL_PASSWORD__
  },
  pgbouncer: {
    password: __PGBOUNCER_PASSWORD__
  }
}

# TBD: How to make this dynamic?
pgbouncer['databases'] = {
  gitlabhq_production: {
    host: __SQL_NODES__[0],
    user: 'pgbouncer',
    password: __PGBOUNCER_PASSWORD__
  }
}

# __GEO_REPLICA_NODE__ = SQLNODE001
# __GEO_TRACKING_NODE__ =  SQLNODE002
# GEO Logcursor should be enbled on tracking DB node
geo_logcursor['enable'] = __HOST_FQDN__ == __GEO_TRACKING_NODE__

if __HOST_FQDN__ == __GEO_REPLICA_NODE__
  postgresql['listen_address'] = __HOST_FQDN__
  postgresql['port'] = 5432
  postgresql['md5_auth_cidr_addresses'] = __OTHER_SQL_NODES__ + __WEB_NODES__ + __SQL_NODES__
  postgresql['trust_auth_cidr_addresses'] = __SQL_NODES__ + __SK_NODES__

  # Database credentials password (defined previously in primary node) replicate same values here as
  # defined in primary node
  postgresql['sql_user_password'] =
    # Database credentials password (defined previously in primary node) replicate same values here as
    # defined in primary node
    gitlab_rails['db_username'] = 'gitlab-psql'
  gitlab_rails['db_password'] = __ROOT_PASSWORD__

  # When running the Geo tracking database on a separate machine, disable it here and allow
  # connections from the tracking database host. And ensure the tracking database IP is in
  # postgresql['md5_auth_cidr_addresses'] above.
  geo_postgresql['enable'] = false
else
  # Set only on non REPLICA DB Nodes
  geo_postgresql['enable'] = __HOST_FQDN__ == __GEO_TRACKING_NODE__
  postgresql['enable'] = false
end

if __HOST_FQDN__ == __GEO_TRACKING_NODE__
  gitlab_rails['db_host'] = __GEO_REPLICA_NODE__
  geo_postgresql['listen_address'] = __HOST_FQDN__
  geo_postgresql['sql_user_password'] = __SQL_USER_PASSWORD__
  geo_secondary['db_fdw'] = true
  geo_postgresql['fdw_external_user'] = 'gitlab_geo_fdw'
  geo_postgresql['fdw_external_password'] = __ROOT_PASSWORD__
  geo_postgresql['md5_auth_cidr_addresses'] = __SQL_NODES__ + __WEB_NODES__ + __SK_NODES__ + __OTHER_SQL_NODES__
end

gitlab_rails['auto_migrate'] = false

# Disable all other services that aren't needed, since we don't have a role
alertmanager['enable'] = false
consul['enable'] = false
gitaly['enable'] = false
gitlab_exporter['enable'] = false
gitlab_workhorse['enable'] = false
nginx['enable'] = false

prometheus['enable'] = false
redis['enable'] = false
redis_exporter['enable'] = false
repmgr['enable'] = false
sidekiq['enable'] = false
puma['enable'] = false
# postgres_exporter['enable'] = false

# PostgresSQl SSL
postgresql['ssl'] = 'on'
postgresql['ssl_ciphers'] = 'HIGH:MEDIUM:+3DES:!aNULL:!SSLv3:!TLSv1'
postgresql['ssl_cert_file'] = '/etc/gitlab/ssl/psql.crt'
postgresql['ssl_key_file'] = '/etc/gitlab/ssl/psql.key'
postgresql['ssl_ca_file'] = '/opt/gitlab/embedded/ssl/certs/cacert.pem'
postgresql['ssl_crl_file'] = nil
postgresql['hot_standby_feedback'] = 'on'

node_exporter['listen_address'] = "#{__HOST_FQDN__}:9100"

# Enable service discovery for Prometheus
consul['enable'] = true
consul['monitoring_service_discovery'] = true
consul['configuration'] = {
  retry_join: __CONSUL_NODES__,
  bind_addr: __PUBLIC_IP_ADDRESS__
}

# Backup Settings
gitlab_rails['manage_backup_path'] = true
gitlab_rails['backup_path'] = '/srv/backup'
gitlab_rails['backup_archive_permissions'] = 0o644
gitlab_rails['backup_pg_schema'] = 'public'

# #####PSQL performance tuning#####
postgresql['shmmax'] =  51_539_607_552
postgresql['shmall'] =  12_582_912
postgresql['shared_buffers'] = '30GB'

postgresql['max_connections'] = 500

postgresql['sql_user'] = 'gitlab-psql'
pgbouncer['default_pool_size'] = '150'

postgres_exporter['enable'] = false
pgbouncer_exporter['enable'] = false
