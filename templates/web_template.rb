# frozen_string_literal: true

# ----------------------------------------------------------------------------
# Gitlab.rb WEB Template Reference
# ----------------------------------------------------------------------------

if __ROLE__ == 'PRIMARY'
  roles %w[geo_primary_role application_role]
  gitlab_rails['geo_node_name'] = __EXTERNAL_HOSTNAME__.to_s
  external_url "https:/#{__EXTERNAL_HOSTNAME__}"
else
  roles %w[geo_secondary_role application_role]
  gitlab_rails['geo_node_name'] = __GEO_NODE_NAME__
  external_url "https:/#{__GEO_EXTERNAL_HOSTNAME__}"
end

# Nginx Config
nginx['enable'] = true
nginx['ssl_certificate'] = '/etc/gitlab/ssl/gitlab.crt'
nginx['ssl_certificate_key'] = '/etc/gitlab/ssl/gitlab.key'
nginx['ssl_ciphers'] =
  'ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:DHE-DSS-AES256-GCM-SHA384:DHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA256:ECDH-RSA-AES256-GCM-SHA384:ECDH-ECDSA-AES256-GCM-SHA384:ECDH-RSA-AES256-SHA384:ECDH-ECDSA-AES256-SHA384:AES256-GCM-SHA384:AES256-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:DHE-DSS-AES128-GCM-SHA256:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES128-SHA256:DHE-DSS-AES128-SHA256:ECDH-RSA-AES128-GCM-SHA256:ECDH-ECDSA-AES128-GCM-SHA256:ECDH-RSA-AES128-SHA256:ECDH-ECDSA-AES128-SHA256:AES128-GCM-SHA256:AES128-SHA256:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES256-SHA:DHE-DSS-AES256-SHA:DHE-RSA-CAMELLIA256-SHA:DHE-DSS-CAMELLIA256-SHA:ECDH-RSA-AES256-SHA:ECDH-ECDSA-AES256-SHA:AES256-SHA:CAMELLIA256-SHA:PSK-AES256-CBC-SHA:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA:DHE-RSA-CAMELLIA128-SHA:DHE-DSS-CAMELLIA128-SHA:ECDH-RSA-AES128-SHA:ECDH-ECDSA-AES128-SHA:AES128-SHA:CAMELLIA128-SHA:PSK-AES128-CBC-SHA'
nginx['ssl_protocols'] = 'TLSv1 TLSv1.1 TLSv1.2'

# nginx trust IP from Loadbalancer
nginx['real_ip_trusted_addresses'] = ['X.Y.W.Z/24']
nginx['real_ip_header'] = 'X-Forwarded-For'
nginx['redirect_http_to_https'] = true
nginx['redirect_http_to_https_port'] = 80

# GitLab Pages
if __ROLE__ == 'PRIMARY'
  pages_external_url "https://gitlab-pages#{__DOMAIN_SUFFIX__}"
else
  pages_external_url "https://gitlab-geo-pages#{__DOMAIN_SUFFIX__}"
end

pages_nginx['redirect_http_to_https'] = true
pages_nginx['ssl_certificate'] = '/etc/gitlab/ssl/pages.crt'
pages_nginx['ssl_certificate_key'] = '/etc/gitlab/ssl/pages.key'
gitlab_pages['enable'] = true
gitlab_pages['inplace_chroot'] = true
gitlab_pages['access_control'] = false
gitlab_pages['listen_proxy'] = 'localhost:8090'
gitlab_pages['redirect_http'] = true
gitlab_pages['domain_config_source'] = 'disk'

# PostgreSQL connection details
gitlab_rails['db_adapter'] = 'postgresql'
gitlab_rails['db_encoding'] = 'unicode'

if __ROLE__ == 'GEO'
  # Configure the connection to the tracking DB.
  # And disable application servers from running tracking databases.
  # Tracking database will be SQL002 (= __SQL_NODES__[1])
  geo_secondary['db_host'] = __DB_TRACKING_HOST__
  geo_secondary['db_password'] = __DB_PASSWORD__
  geo_postgresql['enable'] = false
end

gitlab_rails['db_password'] = __DB_PASSWORD__
gitlab_rails['db_username'] = 'gitlab-psql'

# PGbouncer config
gitlab_rails['db_host'] = "gitlab-#{__SITE_SHORT__}-pgbouncer#{__DOMAIN_SUFFIX__}"
gitlab_rails['db_port'] = '6432'

# Redis connection details
redis['master_name'] = 'gitlab-redis'
redis['master_password'] = __ROOT_PASSWORD__
gitlab_rails['redis_password'] = __ROOT_PASSWORD__
gitlab_rails['redis_sentinels'] = __REDIS_NODES__.map { |rn| { 'host' => rn, 'port' => 26_379 } }

# Ensure UIDs and GIDs match between servers for permissions via NFS
user['uid'] = 9000
user['gid'] = 9000
web_server['uid'] = 9001
web_server['gid'] = 9001
registry['uid'] = 9002
registry['gid'] = 9002

# Enable service discovery for Prometheus
consul['enable'] = true
consul['monitoring_service_discovery'] = true

# with the addresses of the Consul server nodes
consul['configuration'] = {
  retry_join: __CONSUL_NODES__
}

# Gitaly authentication token
gitlab_rails['gitaly_token'] = __GITALY_PASSWORD__
gitaly['enable'] = false
git_data_dirs(__GITALY_DATA_DIRS_TLS__)

# Set the network addresses that the exporters will listen on
node_exporter['listen_address'] = "#{__HOST_FQDN__}:9100"
gitlab_workhorse['prometheus_listen_addr'] = "#{__HOST_FQDN__}:9229"
gitlab_workhorse['proxy_headers_timeout'] = '3m0s'

# Sidekiq
sidekiq['enable'] = false

puma['listen'] = '0.0.0.0'
puma['per_worker_max_memory_mb'] = 2048

gitlab_rails['env'] = {
  'GITLAB_RAILS_RACK_TIMEOUT' => 120,
  'PLANTUML_ENCODING' => 'deflate'
}

letsencrypt['enable'] = false

# Webhook Settings Number of seconds to wait for HTTP response after sending webhook HTTP POST  request (default: 10)
gitlab_rails['webhook_timeout'] = 10 # For setting up LDAP
gitlab_rails['ldap_enabled'] = true

# ##LDAP_PASSWORD###
gitlab_rails['ldap_servers'] = YAML.safe_load <<-LDAP_CONFIG
  main:
    label: 'LDAP Login'
    host: __LDAP_SERVER__
    port: __LDAP_PORT__
    uid: 'sAMAccountName'
    encryption: 'simple_tls'
    bind_dn: __LDAP_BIND_DN__
    password: __LDAP_PASSWORD__
    verify_certificates: false
    active_directory: true
    allow_username_or_email_login: false
    block_auto_created_users: false
    base: __LDAP_BASE__
    user_filter: __LDAP_FILTER__
    group_base: __LDAP_FILTER__
    admin_group: 'GitLab Support'
    sync_ssh_keys: 'false'
LDAP_CONFIG

# Nginx custom config
nginx['custom_nginx_config'] = 'include /var/opt/gitlab/nginx/conf/ipfilter.conf;'

# Email Settings
gitlab_rails['gitlab_email_enabled'] = true
gitlab_rails['gitlab_email_from'] = ''
gitlab_rails['gitlab_email_display_name'] = 'GitLab'
gitlab_rails['gitlab_email_reply_to'] = ''
gitlab_rails['gitlab_email_subject_suffix'] = '[GitLab]'

# GitLab email server settings

# Backup Settings

# The duration in seconds to keep backups before they are allowed to be deleted

# Job Artifacts

# Git LFS

# GitLab Shell settings for GitLab

# GitLab application settings

# Download location When a user clicks e.g. 'Download zip' on a project, a temporary zip file is created in the following directory.

# Add the monitoring node's IP address to the monitoring whitelist and allow it to scrape the NGINX metrics.
# Replace placeholder `monitoring.gitlab.example.com` with the address and/or subnets gathered from the monitoring node(s).
# TBD: Remove __MON2_NODE_IP__.
gitlab_rails['monitoring_whitelist'] = ['127.0.0.0/8', __MON1_NODE_IP__, __MON2_NODE_IP__]
nginx['status']['options']['allow'] = [__MON1_NODE_IP__, __PUBLIC_IP_ADDRESS__, '127.0.0.0/8']

# GitLab user privileges

# Default Theme

# Default project feature settings

# GitLab Logging
logging['logrotate_size'] = '1M'
logging['logrotate_maxsize'] = '2M'
logging['logrotate_rotate'] = 30
logging['logrotate_compress'] = 'compress'
logging['logrotate_method'] = 'copytruncate'
logging['logrotate_dateformat'] = '-%Y-%m-%d-%s'

# Auxiliary cron jobs applicable to GitLab EE only

# Auxiliary jobs Periodically executed jobs, to self-heal Gitlab, do external synchronizations, etc.

# Git trace log file.

# Disable automatic database migrations
gitlab_rails['auto_migrate'] = false

gitlab_rails['rake_cache_clear'] = false
# Service Desk configuration

gitlab_rails['incoming_email_enabled'] = true
gitlab_rails['incoming_email_address'] = "incoming+%<key>s@gitlab-#{__SITE_SHORT__}-servicedesk#{__DOMAIN_SUFFIX__}"
gitlab_rails['incoming_email_email'] = 'incoming'
gitlab_rails['incoming_email_password'] = __EMAIL_PASSWORD__
gitlab_rails['incoming_email_host'] = "gitlab-#{__SITE_SHORT__}-servicedesk#{__DOMAIN_SUFFIX__}"
gitlab_rails['incoming_email_port'] = 143
gitlab_rails['incoming_email_ssl'] = true
gitlab_rails['incoming_email_start_tls'] = false
gitlab_rails['incoming_email_mailbox_name'] = 'inbox'
gitlab_rails['incoming_email_idle_timeout'] = 60

# gitlab plantuml feature redirection
nginx['custom_gitlab_server_config'] =
  "location /-/plantuml/ { \n rewrite ^/-/(plantuml.*) /$1 break;\n proxy_cache off; \n proxy_pass https://gitlab-#{__SITE_SHORT__}-servicedesk#{__DOMAIN_SUFFIX__}:8443/plantuml; \n}\n"
