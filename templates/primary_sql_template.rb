# frozen_string_literal: true

# ----------------------------------------------------------------------------
# Role
# ----------------------------------------------------------------------------
roles %w[geo_primary_role postgres_role]

# ----------------------------------------------------------------------------
# PostgreSQL configuration
# ----------------------------------------------------------------------------
postgresql['listen_address'] = __HOST_FQDN__
postgresql['hot_standby'] = 'on'
postgresql['wal_level'] = 'replica'

# Disable automatic database migrations
gitlab_rails['auto_migrate'] = false

# PostgresSQl
postgresql['ssl'] = 'on'
postgresql['ssl_ciphers'] = 'HIGH:MEDIUM:+3DES:!aNULL:!SSLv3:!TLSv1'
postgresql['ssl_cert_file'] = '/etc/gitlab/ssl/psql.crt'
postgresql['ssl_key_file'] = '/etc/gitlab/ssl/psql.key'
postgresql['ssl_ca_file'] = '/opt/gitlab/embedded/ssl/certs/cacert.pem'
postgresql['ssl_crl_file'] = nil
postgresql['sql_user'] = 'gitlab-psql'
postgresql['sql_user_password'] = __SQL_USER_PASSWORD__
patroni['postgresql']['max_wal_senders'] = 10
patroni['postgresql']['max_replication_slots'] = 6

# PGBOUNCER_PASSWORD_HASH with a generated md5 value
postgresql['pgbouncer_user_password'] = __PGBOUNCER_USER_PASSWORD__

# Add database sidekiq(which is running on different nodes) Network Address
postgresql['trust_auth_cidr_addresses'] = ['127.0.0.1/32'] +  __SQL_NODES__ + __SK_NODES__

# Add database  Network Address
postgresql['md5_auth_cidr_addresses'] = __ALL_PGBOUNCER_NODES__ + __ALL_WEB_NODES__ + __ALL_SQL_NODES__
patroni['allowlist'] = ['127.0.0.1/32'] + __SQL_NODES__
# #####PSQL performance tuning#####
postgresql['shmmax'] =  51_539_607_552
postgresql['shmall'] =  12_582_912
postgresql['shared_buffers'] = '30GB'

# ###Databse Pool Connection#####
postgresql['max_connections'] = 500

# ----------------------------------------------------------------------------
# Pgbouncer configuration
# ----------------------------------------------------------------------------
pgbouncer['enable'] = true
pgbouncer['auth_type'] = 'trust'
pgbouncer['admin_users'] = %w[pgbouncer gitlab-consul]
pgbouncer['users'] = {
  'gitlab-consul': {
    password: __CONSUL_PASSWORD__
  },
  pgbouncer: {
    password: __PGBOUNCER_PASSWORD__
  }
}
# #######PG Bouncer############
pgbouncer['default_pool_size'] = '150'

# ----------------------------------------------------------------------------
# Consul Agent Configruation
# ----------------------------------------------------------------------------
consul['services'] = %w[postgresql]
consul['watchers'] = %w[postgresql]

# Monitoring configuration
consul['configuration'] = {
  retry_join: __CONSUL_NODES__,
  bind_addr: __PUBLIC_IP_ADDRESS__
}

# Enable service discovery for Prometheus
consul['monitoring_service_discovery'] = true

# ----------------------------------------------------------------------------
# Repmgr Configuration
# ----------------------------------------------------------------------------
# Add database  Network Address
# repmgr['trust_auth_cidr_addresses'] = __SQL_NODES__

# repmgr - this should not be set normally
# repmgr['master_on_initialization'] = false

# Patroni
patroni['username'] = 'gitlab-patroni'
patroni['password'] = __DB_PASSWORD__
patroni['enable'] = true
patroni['replication_slots'] = {
  __GEO_NODE_NAME__ => { 'type' => 'physical' }
}

# ----------------------------------------------------------------------------
# Exporters Configuration
# ----------------------------------------------------------------------------
node_exporter['listen_address'] = "#{__HOST_FQDN__}:9100"
postgres_exporter['listen_address'] = "#{__HOST_FQDN__}:9187"
pgbouncer_exporter['listen_address'] = "#{__HOST_FQDN__}:9188"

# ----------------------------------------------------------------------------
# Gitlab Rails Configuration
# ----------------------------------------------------------------------------
# Backup Settings
gitlab_rails['manage_backup_path'] = true
gitlab_rails['backup_path'] = '/srv/backup'
gitlab_rails['backup_archive_permissions'] = 0o644
gitlab_rails['backup_pg_schema'] = 'public'
