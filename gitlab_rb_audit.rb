#!/usr/bin/ruby

# frozen_string_literal: true

# Audit Gitlab Environment - Validate gitlab.rb files.
#
require 'socket'
require 'optparse'

require_relative '../environment'
require_relative '../gitlab_rb_loader'

# -----------------------------------------------------------
# MAIN
# -----------------------------------------------------------
options = { role: 'PRIMARY', state: 'PROD', rbfile: '/etc/gitlab/gitlab.rb' }
OptionParser.new do |opts|
  opts.banner = 'Usage: gitlab_rb_audit.rb [-r PRIMARY|GEO] [-s PROD|PRE-FAILOVER]'
  opts.on('-rROLE', '--role=ROLE', 'Role - one of PRIMARY or GEO. Defaults to PRIMARY') do |r|
    raise "ERROR: Invalid role #{r}" unless %w[PRIMARY GEO].include?(r)

    options[:role] = r
  end

  opts.on('-sSTATE', '--state=STATE', 'State PROD or PRE-FAILOVER. Defaults to PROD') do |s|
    raise "Audit for STATE '#{options[:state]}' not implemented yet" unless options[:state] == 'PROD'

    options[:state] = s
  end

  opts.on('-fRBFILE', '--rbfile=RBFILE', 'Path of gitlab.rb. Defaults to /etc/gitlab/gitlab.rb') do |f|
    raise "gitlab.rb file '#{s}' not found or readable" unless File.exist?(options[:rbfile])

    options[:rbfile] = f
  end
end.parse!

cfg_ctx = Environment.new(options[:role], options[:state])

actual_rb = GitlabRbLoader.new(options[:rbfile])
reference_rb = GitlabRbLoader.new(cfg_ctx.reference_rb_file, cfg_ctx)
errors = GitlabRbLoader.audit(reference_rb, actual_rb)

if errors.empty?
  puts "#{Socket.gethostname}: AUDIT SUCCESS"
else
  puts "#{Socket.gethostname}: ERROR - AUDIT FAILED."
  errors.each { |e| puts "  #{e.gsub("\t", '    ')}" }
end
