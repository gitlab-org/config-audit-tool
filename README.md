## Introduction

Tool for auditing `gitlab.rb` files in Clustered Gitlab Instance.


## Reference Config Files
  The reference/ folder should contain reference files used for auditing.  Audit is done with an expected role of PRIMARY or GEO.
  Each host_type (web, sql, git, redis, sk, consul) can either have a single reference file '<host_type>_remplate.rb' or a specfic one each for primary and geo roles.  e.g. primary_sql_template.rb, geo_sql_template.rb.
  The reference files use Envionment variable from audit tool using __VAR__ notation.  e.g. __ROLE__ will be set to either "PRIMARY" or "GEO" and can be used to provide conditional reference configuration for primary and geo.


## Environment
  The script assumes following environment. This needs to be abstracted/refactored for generalization. The envioronment is dynamically created as part the Environmnet class and defines __VAR__ vars that can be accessed from reference files.

### Sites and Roles
  The gitlab deployment is setup to be across two sites (in the code they are referred to as site_a, site_b context vars). At any given time the one of the two sites would be primary, the other will be geo.

### State
  The state option is used describe transition states during a failover.
    ["PROD", "PHASE-1B", "PRE-FAILOVER"]

### Hostnames
  All hostnames are expected to have format "<site>sgl<host_type><host_id>"
  site = string that reprsents the either primary or geo location.
  host_id = 3 digt number.  web: 001..008, sk: 001..004, sql: 001..003, git: 001..005, inf: 001..006, mon:001
  host_type = web|inf|sk|sql|mon|git
    inf001..3 are consul nodes
    inf004..6 are redis nodes

###  Gitaly repo names - assumes 4 repos.
  ["default", "repos2", "repos3", "repos4"]

### Secrets
  Reference of secrets in the gitlab.rb file are to be stored and can be loaded. Utility module glcrypt.rb has AES encrypt/decrypt utility
  The implmentation of the secret management is not incldued in this code.
